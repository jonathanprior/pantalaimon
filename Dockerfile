FROM debian:bullseye
RUN apt-get update && apt-get install -y pantalaimon
VOLUME /data
ENTRYPOINT ["pantalaimon"]
CMD ["-c", "/data/pantalaimon.conf", "--data-path", "/data"]
